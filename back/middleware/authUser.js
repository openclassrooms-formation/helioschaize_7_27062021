const emailValidator = require("email-validator");
const passwordValidator = require("password-validator");

/* *************************************** *
 * Middleware for valid email and password *
 * *************************************** */
exports.valid = (req, res, next) => {
  const passwordSchema = new passwordValidator();
  passwordSchema
      .is().min(8)
      .has().uppercase()
      .has().lowercase()
      .has().symbols()
      .has().digits(1);

  if (!emailValidator.validate(req.body.email) || !passwordSchema.validate(req.body.password))
  {
    return res.status(400).send({ error: "Merci de vérifier ton adresse mail, ton mot de passe doit contenir au minum 8 lettres avec des minuscules, majuscules, symboles spéciaux et chiffres  "});
  }
  else if (emailValidator.validate(req.body.email) || passwordSchema.validate(req.body.password))
  {
    next();
  }
};

/* ****************************************** *
 *  Middleware for check validity of Username *
 * ****************************************** */
exports.checkPseudo = (req, res, next) => {
  const regex = /^[a-zA-Z0-9_]{3,30}$/;
  const pseudo = req.body.pseudo;
  if (regex.test(pseudo) === true)
  {
    next();
  }
  else
  {
    return res.status(400).send({ error: "Votre pseudo doit être de 3 caractères minimum et 30 maximum, sont acceptées les lettres, chiffres et underscore (_)  " });
  }
};
