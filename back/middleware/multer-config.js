const multer = require("multer");

/* *********************************** *
 * Dictionary of available image types *
 * *********************************** */
const MIME_TYPES = {
  // notre dictionnaire d'extensions
  "image/jpg": "jpg",
  "image/jpeg": "jpg",
  "image/png": "png",
  "image.gif": "gif",
  "image.webp": "webp",
};

/* **************************** *
 * Middleware for image storage *
 * **************************** */
const storage = multer.diskStorage({
  destination: (req, file, callback) =>
  {
    callback(null, "upload");
  },
  filename: (req, file, callback) =>
  {
    const name = file.originalname.replace(/\.[^/.]+$/, "");
    const extension = MIME_TYPES[file.mimetype];
    callback(null, name + Date.now() + "." + extension);
  },
});
module.exports = multer({ storage: storage }).single("image");
