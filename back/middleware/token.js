const JWT = require("jsonwebtoken");

/* ********************** *
 * Function to sign token *
 * ********************** */
function issueJWT(user) {
  const id = user.id;
  const expiresIn = "24H";
  const payload = {
    sub: id,
    iat: Date.now(),
  };
  const signedToken = JWT.sign(payload, `${process.env.TOKENKEY}`, { expiresIn: expiresIn });
  return {
    token: "Bearer " + signedToken,
    expires: expiresIn,
  };
}

/* *************************** *
 * Obtain userId with webToken *
 * *************************** */
function getUserId(req) {
  // on vérifie le userId du token
  const token = req.headers.authorization.split(" ")[1];
  const decodedToken = JWT.verify(token, `${process.env.TOKENKEY}`);
  const userId = decodedToken.sub;
  return userId;
}

module.exports.issueJWT = issueJWT;
module.exports.getUserId = getUserId;
