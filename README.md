# Projet 7 - Groupomania !

7ème et dernier projet de la formation développeur web d'Openclassrooms.
Créer un réseau social d'entreprise.
La stack utilisée pour ce projet:

- VueJs + vuex + vuetify
- NodeJs + express + sequelize
- Mysql

## Frontend

Ouvrir le dossier Frontend dans le terminal de votre éditeur puis exécuter la commande:

    npm install

puis

    npm start

si le navigateur ne s'ouvre pas automatiquement allez à :

- http://localhost:8080/

## Backend

Ouvrir le dossier Backend dans le terminal de votre éditeur puis exécuter la commande:

    npm install

puis

    npm start ou nodemon serve

## Base de données

Se connecter au serveur **MySql** de votre choix, un accès via phpMyAdmin sera plus simple.
Puis importer le fichier socialnetwork.sql s'il vous a été fourni :

    mysql -u root -p < socialnetwork.sql

Il faut remplacer `socialnetwork.sql` par le chemin du fichier dans votre machine.
Au lancement de l'application un compte administrateur est automatiquement créé dont les identifiants sont:
- e-mail: `admin@mail.com`
- mot de passe: `Moderator%1`

## Utilisation

Pour s'inscrire sur le social network de Groupomania, il vous faut renseigner :

- Un pseudo (entre 3 et 30 caractères)
- Une adresse mail valide
- Un mot de passe (min. 8 caractères, chiffres, majuscules, minuscules, et symboles).
  Vous pouvez par la suite modifier votre profil (pseudo, bio, photo) en allant sur votre profil. Votre compte peut être supprimé par vous-même ainsi que par l'administrateur.

Une fois connecté vous pouvez voir les publications des utilisateurs et publier au choix:

- un statut
- un statut + une image ou un gif
  Ces publications peuvent être likées, commentées, modifiées, supprimées. Le modérateur peut les supprimer.
